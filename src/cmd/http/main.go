package main

import (
	"context"
	"github.com/go-playground/validator/v10"
	"github.com/gorilla/handlers"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/belov/pdp/internal/config"
	"gitlab.com/belov/pdp/internal/pkg/logger"
	"gitlab.com/belov/pdp/internal/service/one"
	"gitlab.com/belov/pdp/internal/service/three"
	"gitlab.com/belov/pdp/internal/service/two"
	api "gitlab.com/belov/pdp/internal/transport/http"
	"go.uber.org/zap"
)

// @title		PDP
// @version	1.0
//
// @BasePath	/api
func main() {
	if err := run(); err != nil {
		log.Fatal(err)
	}

	os.Exit(1)
}

func run() error {
	var cfg config.Config
	err := config.Read(&cfg)
	if err != nil {
		return err
	}

	log := logger.NewLogger()

	buttonSolver := one.NewButtonSolver(log)
	trimmerSolver := one.NewTrimmerSolver(log)
	creditSolver := one.NewCreditSolver(log)
	matrixSolver := one.NewMatrixSolver(log)
	diamondSolver := two.NewDiamondSolver(log)
	kgSolver := three.NewKindergardenSolver(log)

	validate := validator.New(validator.WithRequiredStructEnabled())

	httpServer := api.NewHTTPServer(buttonSolver, creditSolver, trimmerSolver, matrixSolver, diamondSolver, kgSolver,
		validate, log)

	mux := http.NewServeMux()

	mux.HandleFunc("/api/one/button", httpServer.Button)
	mux.HandleFunc("/api/one/credit", httpServer.Credit)
	mux.HandleFunc("/api/one/matrix", httpServer.Matrix)
	mux.HandleFunc("/api/one/trim", httpServer.Trim)
	mux.HandleFunc("/api/two/diamond", httpServer.Diamond)
	mux.HandleFunc("/api/three/kindergarden", httpServer.Kindergarden)

	srv := http.Server{
		Addr:    ":" + cfg.HTTPAddr,
		Handler: handlers.LoggingHandler(os.Stdout, mux),
	}

	stopped := make(chan struct{})
	go func() {
		sigint := make(chan os.Signal, 1)
		signal.Notify(sigint, os.Interrupt, syscall.SIGTERM)
		<-sigint
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()
		if err := srv.Shutdown(ctx); err != nil {
			log.Error("HTTP Server Shutdown Error:", zap.Error(err))
		}
		close(stopped)
	}()

	log.Info("starting HTTP server:", zap.String("port", cfg.HTTPAddr))

	if err := srv.ListenAndServe(); err != http.ErrServerClosed {
		log.Error("HTTP server ListenAndServe Error:", zap.Error(err))
	}

	<-stopped

	return nil
}
