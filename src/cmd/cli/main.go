/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package main

import "gitlab.com/belov/pdp/internal/transport/cli"

func main() {
	cli.Execute()
}
