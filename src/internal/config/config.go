package config

import (
	"github.com/joho/godotenv"
	"github.com/kelseyhightower/envconfig"
)

type Config struct {
	HTTPAddr string `envconfig:"port" required:"true"`
}

func Read(config *Config) error {
	err := godotenv.Load()
	if err != nil {
		return err
	}

	err = envconfig.Process("", config)
	if err != nil {
		return err
	}

	return nil
}
