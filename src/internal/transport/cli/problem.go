package cli

import (
	"fmt"
	"strconv"

	"gitlab.com/belov/pdp/internal/service/one"
	"gitlab.com/belov/pdp/internal/service/three"
	"gitlab.com/belov/pdp/internal/service/two"

	"github.com/spf13/cobra"
)

func problemSub() []*cobra.Command {
	cmdButton := &cobra.Command{
		Use:  "button",
		Args: cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			fmt.Println(args)
			solver := one.NewButtonSolver(l)
			result := solver.Solve(args[0])
			fmt.Println(result)

			return nil
		},
	}

	cmdCredit := &cobra.Command{
		Use:  "credit",
		Args: cobra.ExactArgs(2),
		RunE: func(cmd *cobra.Command, args []string) error {
			credit, err := strconv.ParseFloat(args[0], 64)
			if err != nil {
				return err
			}
			percent, err := strconv.ParseFloat(args[1], 64)
			if err != nil {
				return err
			}
			fmt.Println(args)
			solver := one.NewCreditSolver(l)
			result := solver.Solve(credit, percent)
			fmt.Printf("%0.2f", result)
			fmt.Println()

			return nil
		},
	}

	cmdMatrix := &cobra.Command{
		Use:  "matrix",
		Args: cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			size, err := strconv.Atoi(args[0])
			if err != nil {
				return err
			}

			solver := one.NewMatrixSolver(l)
			result := solver.Solve(size)
			for _, m := range result {
				fmt.Println(m)
			}

			return nil
		},
	}

	cmdTrim := &cobra.Command{
		Use:  "trim",
		Args: cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			solver := one.NewTrimmerSolver(l)
			result := solver.Solve(args[0])
			fmt.Println(result)

			return nil
		},
	}

	cmdDiamond := &cobra.Command{
		Use:  "diamond",
		Args: cobra.MinimumNArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			size, err := strconv.Atoi(args[0])
			if err != nil {
				return err
			}

			bg := byte(' ')
			if len(args) > 1 {
				bg = []byte(args[1])[0]
			}
			solver := two.NewDiamondSolver(l, bg)
			result := solver.Solve(size)
			fmt.Println(result)

			return nil
		},
	}

	cmdKindergarned := &cobra.Command{
		Use:  "kindergarden",
		Args: cobra.ExactArgs(2),
		RunE: func(cmd *cobra.Command, args []string) error {
			n, err := strconv.Atoi(args[0])
			if err != nil {
				return err
			}

			k, err := strconv.Atoi(args[1])
			if err != nil {
				return err
			}

			solver := three.NewKindergardenSolver(l)
			result := solver.Solve(n, k)
			fmt.Println(result)

			return nil
		},
	}
	return []*cobra.Command{cmdButton, cmdCredit, cmdMatrix, cmdTrim, cmdDiamond, cmdKindergarned}
}

func init() {
	cmdProblem := &cobra.Command{
		Use:   "problem",
		Short: "Solve a problem",
		RunE: func(cmd *cobra.Command, args []string) error {
			_ = cmd.Help()
			return nil
		},
	}

	cmdProblem.AddCommand(problemSub()...)
	rootCmd.AddCommand(cmdProblem)
}
