package cli

import (
	"fmt"

	"github.com/spf13/cobra"
)

const version = 1

func init() {
	rootCmd.AddCommand(versionCmd)
}

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Print the version number",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Printf("v%d", version)
		fmt.Println()
	},
}
