/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package cli

import (
	"os"

	"gitlab.com/belov/pdp/internal/pkg/logger"

	"github.com/spf13/cobra"
)

var (
	l = logger.NewLogger()
)

var rootCmd = &cobra.Command{
	Use: "pdp",
	RunE: func(cmd *cobra.Command, args []string) error {
		_ = cmd.Help()
		return nil
	},
}

func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}
