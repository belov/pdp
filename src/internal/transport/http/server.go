package http

import (
	"encoding/json"
	"errors"
	"net/http"
	"strconv"

	"github.com/go-playground/validator/v10"
	"gitlab.com/belov/pdp/internal/pkg/httputil"
	"go.uber.org/zap"
)

type ButtonSolver interface {
	Solve(str string) int
}

type CreditSolver interface {
	Solve(credit float64, percent float64) float64
}

type MatrixSolver interface {
	Solve(size int) [][]int
}

type TrimSolver interface {
	Solve(str string) string
}

type DiamondSolver interface {
	Solve(length int) string
}

type KindergardenSolver interface {
	Solve(pcs, modems int) int
}

type Server struct {
	l         *zap.Logger
	btn       ButtonSolver
	credit    CreditSolver
	trim      TrimSolver
	matrix    MatrixSolver
	diamond   DiamondSolver
	kg        KindergardenSolver
	validator *validator.Validate
}

func NewHTTPServer(
	btn ButtonSolver,
	credit CreditSolver,
	trim TrimSolver,
	matrix MatrixSolver,
	diamond DiamondSolver,
	kg KindergardenSolver,
	validator *validator.Validate,
	l *zap.Logger,
) Server {
	return Server{
		l:         l,
		btn:       btn,
		credit:    credit,
		trim:      trim,
		matrix:    matrix,
		diamond:   diamond,
		kg:        kg,
		validator: validator,
	}
}

func (s Server) Button(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		httputil.NewError(w, http.StatusBadRequest, errors.New(http.StatusText(http.StatusMethodNotAllowed)))
		return
	}

	q := ButtonRequestQuery{
		Str: r.URL.Query().Get("string"),
	}

	if err := s.validator.Struct(&q); err != nil {
		httputil.NewError(w, http.StatusBadRequest, errors.New(http.StatusText(http.StatusBadRequest)))
		return
	}

	result := s.btn.Solve(q.Str)

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	_ = json.NewEncoder(w).Encode(map[string]any{
		"result": result,
	})
}

func (s Server) Credit(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		httputil.NewError(w, http.StatusBadRequest, errors.New(http.StatusText(http.StatusMethodNotAllowed)))
		return
	}

	amount, _ := strconv.ParseFloat(r.URL.Query().Get("amount"), 64)
	percent, _ := strconv.ParseFloat(r.URL.Query().Get("percent"), 64)

	q := CreditRequestQuery{
		Amount:  amount,
		Percent: percent,
	}

	if err := s.validator.Struct(&q); err != nil {
		httputil.NewError(w, http.StatusBadRequest, errors.New(http.StatusText(http.StatusBadRequest)))
		return
	}

	result := s.credit.Solve(q.Amount, q.Percent)

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	_ = json.NewEncoder(w).Encode(map[string]any{
		"result": result,
	})
}

func (s Server) Matrix(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		httputil.NewError(w, http.StatusBadRequest, errors.New(http.StatusText(http.StatusBadGateway)))
		return
	}

	size, _ := strconv.ParseInt(r.URL.Query().Get("size"), 10, 64)

	q := MatrixRequestQuery{
		Size: int(size),
	}

	if err := s.validator.Struct(&q); err != nil {
		httputil.NewError(w, http.StatusBadRequest, errors.New(http.StatusText(http.StatusBadRequest)))
		return
	}

	result := s.matrix.Solve(q.Size)

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	_ = json.NewEncoder(w).Encode(map[string]any{
		"result": result,
	})
}

func (s Server) Trim(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		httputil.NewError(w, http.StatusBadRequest, errors.New(http.StatusText(http.StatusMethodNotAllowed)))
		return
	}

	q := TrimRequestQuery{
		Str: r.URL.Query().Get("string"),
	}
	if err := s.validator.Struct(&q); err != nil {
		httputil.NewError(w, http.StatusBadRequest, errors.New(http.StatusText(http.StatusBadRequest)))
		return
	}

	result := s.trim.Solve(q.Str)
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	_ = json.NewEncoder(w).Encode(map[string]any{
		"result": result,
	})
}
func (s Server) Diamond(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		httputil.NewError(w, http.StatusBadRequest, errors.New(http.StatusText(http.StatusMethodNotAllowed)))
		return
	}

	size, _ := strconv.ParseInt(r.URL.Query().Get("size"), 10, 64)

	q := DiamondRequestQuery{
		Size: int(size),
	}
	if err := s.validator.Struct(&q); err != nil {
		httputil.NewError(w, http.StatusBadRequest, errors.New(http.StatusText(http.StatusBadRequest)))
		return
	}

	result := s.diamond.Solve(q.Size)
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	_ = json.NewEncoder(w).Encode(map[string]any{
		"result": result,
	})
}
func (s Server) Kindergarden(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		httputil.NewError(w, http.StatusBadRequest, errors.New(http.StatusText(http.StatusMethodNotAllowed)))
		return
	}

	pcs, _ := strconv.ParseInt(r.URL.Query().Get("pcs"), 10, 64)
	modems, _ := strconv.ParseInt(r.URL.Query().Get("modems"), 10, 64)

	q := KindergardenRequestQuery{
		PCs:    int(pcs),
		Modems: int(modems),
	}
	if err := s.validator.Struct(&q); err != nil {
		httputil.NewError(w, http.StatusBadRequest, errors.New(http.StatusText(http.StatusBadRequest)))
		return
	}

	result := s.kg.Solve(q.PCs, q.Modems)
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	_ = json.NewEncoder(w).Encode(map[string]any{
		"result": result,
	})
}
