package http

type ResultResponse[T any] struct {
	Result T `json:"result"`
}

func NewResultResponse[T any](payload T) ResultResponse[T] {
	return ResultResponse[T]{
		Result: payload,
	}
}
