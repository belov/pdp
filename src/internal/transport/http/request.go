package http

type ButtonRequestQuery struct {
	Str string `form:"string" validate:"required,alpha"`
}

type CreditRequestQuery struct {
	Amount  float64 `form:"amount" validate:"required,number"`
	Percent float64 `form:"percent" validate:"required,number"`
}

type MatrixRequestQuery struct {
	Size int `form:"size" validate:"required,number,min=1"`
}

type TrimRequestQuery struct {
	Str string `form:"string" validate:"alpha"`
}

type DiamondRequestQuery struct {
	Size int `form:"size" validate:"required,number"`
}

type KindergardenRequestQuery struct {
	PCs    int `form:"pcs" validate:"required,number"`
	Modems int `form:"modems" validate:"required,number"`
}
