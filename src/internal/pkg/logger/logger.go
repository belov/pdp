package logger

import (
	"os"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func NewLogger() *zap.Logger {
	config := zap.NewProductionEncoderConfig()
	consoleEncoder := zapcore.NewJSONEncoder(config)
	defaultLogLevel := zapcore.InfoLevel
	if os.Getenv("MODE") == "debug" {
		config = zap.NewDevelopmentEncoderConfig()
		config.EncodeLevel = zapcore.CapitalColorLevelEncoder
		consoleEncoder = zapcore.NewConsoleEncoder(config)
		defaultLogLevel = zapcore.DebugLevel
	}
	core := zapcore.NewTee(
		zapcore.NewCore(consoleEncoder, zapcore.AddSync(os.Stdout), defaultLogLevel),
	)
	stackTraceLogLevel := defaultLogLevel + 2

	return zap.New(core, zap.AddStacktrace(stackTraceLogLevel))

}
