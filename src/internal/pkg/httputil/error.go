package httputil

import (
	"encoding/json"
	"net/http"
)

func NewError(w http.ResponseWriter, status int, err error) {
	er := HTTPError{
		Code:    status,
		Message: err.Error(),
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusBadRequest)
	_ = json.NewEncoder(w).Encode(er)
}

type HTTPError struct {
	Message string `json:"message" example:"status bad request"`
	Code    int    `json:"code" example:"400"`
}
