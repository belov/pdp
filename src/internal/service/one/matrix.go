package one

import (
	"fmt"
	"go.uber.org/zap"
)

type matrixSolver struct {
	l *zap.Logger
}

func NewMatrixSolver(l *zap.Logger) *matrixSolver {
	return &matrixSolver{l: l}
}

func (s *matrixSolver) Solve(size int) [][]int {
	// n == 2
	// 1 1
	// 1 2

	// n == 3
	// 1 1 1
	// 1 2 3
	// 1 3 6

	matrix := make([][]int, size)
	for i := range matrix {
		matrix[i] = make([]int, size)
	}

	matrix[0][0] = 1
	for i := 1; i < size; i++ {
		matrix[i][0] = 1
		matrix[0][i] = 1
	}

	s.l.Debug("Created matrix")

	for i := 1; i < size; i++ {
		for j := 1; j < size; j++ {
			matrix[i][j] = matrix[i][j-1] + matrix[i-1][j]
		}
		s.l.Debug(fmt.Sprintf("Gone through %d row", i))
	}

	return matrix
}
