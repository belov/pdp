package one

import (
	"gitlab.com/belov/pdp/internal/pkg/ascii"
	"go.uber.org/zap"
)

type buttonsSolver struct {
	l *zap.Logger
}

func NewButtonSolver(l *zap.Logger) *buttonsSolver {
	return &buttonsSolver{l: l}
}

func (s *buttonsSolver) Solve(str string) int {
	sum := 0

	for i := 0; i < len(str); i++ {
		sum += int(str[i])
	}
	sum -= ascii.Offset * len(str)
	return sum
}
