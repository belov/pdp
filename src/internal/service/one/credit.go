package one

import (
	"math"

	"go.uber.org/zap"
)

type creditSolver struct {
	l *zap.Logger
}

func NewCreditSolver(l *zap.Logger) *creditSolver {
	return &creditSolver{l: l}
}

func (s *creditSolver) Solve(credit float64, percent float64) float64 {
	if percent > 1.0 && percent < 2 {
		percent -= 1.0
	}
	paymentsCount := 3.0
	totalPercent := math.Pow(1.0+percent, paymentsCount)
	annualPayment := credit * percent * totalPercent / (totalPercent - 1.0)
	return annualPayment
}
