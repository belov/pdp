package one

import (
	"fmt"
	"strings"

	"go.uber.org/zap"
)

type trimSolver struct {
	l *zap.Logger
}

func NewTrimmerSolver(l *zap.Logger) *trimSolver {
	return &trimSolver{l: l}
}

func (s *trimSolver) Solve(str string) string {
	var trimmed strings.Builder

	for i := 0; i < len(str); i++ {
		s.l.Debug(fmt.Sprintf("Current sybmol: %s", string(str[i])))

		if trimmed.Len() == 0 && str[i] == ' ' {
			continue
		}
		if str[i] != ' ' {
			trimmed.WriteByte(str[i])
			continue
		}

		if len(str)-1 != i {
			if str[i+1] == ' ' {
				continue
			}
		} else {
			break
		}

		trimmed.WriteByte(str[i])
	}
	fmt.Println(trimmed.Len())

	return trimmed.String()
}
