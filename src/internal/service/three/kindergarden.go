package three

import (
	"go.uber.org/zap"
)

type kgSolver struct {
	l *zap.Logger
}

func NewKindergardenSolver(l *zap.Logger) *kgSolver {
	return &kgSolver{l: l}
}

func (s *kgSolver) Solve(pcs, modems int) int {
	installedOn := 1
	hours := 0

	for i := 0; installedOn < pcs; {
		if installedOn < modems {
			i++
			installedOn += i
		} else {
			installedOn += modems
		}

		hours++
	}
	return hours
}
