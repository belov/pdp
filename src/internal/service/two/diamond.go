package two

import (
	"fmt"
	"strings"

	"go.uber.org/zap"
)

type diamondSolver struct {
	l            *zap.Logger
	binaryToByte map[rune]byte
}

func NewDiamondSolver(l *zap.Logger, background ...byte) *diamondSolver {
	s := &diamondSolver{l: l, binaryToByte: map[rune]byte{
		'0': ' ',
		'1': '*',
	}}
	if len(background) > 0 {
		s.binaryToByte['0'] = background[0]
	}

	return s
}

func (s *diamondSolver) Solve(length int) string {
	var builder strings.Builder
	builder.Grow((length*2+1)*(length*2+1) + length)

	mask := 1 << length
	left := mask
	right := mask
	for i := 0; i < length; i++ {
		for _, r := range fmt.Sprintf("'%0*b'", length*2+1, left|right) {
			builder.WriteByte(s.binaryToByte[r])
		}
		left <<= 1
		right >>= 1
		builder.WriteByte('\n')
	}
	left >>= 2
	right <<= 2
	for i := 1; i < length; i++ {
		for _, r := range fmt.Sprintf("'%0*b'", length*2+1, left|right) {
			builder.WriteByte(s.binaryToByte[r])
		}
		left >>= 1
		right <<= 1
		builder.WriteByte('\n')
	}
	return builder.String()
}
